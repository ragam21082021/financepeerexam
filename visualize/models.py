from pyexpat import model
from django.db import models

# Create your models here.


class visuvalizeData(models.Model):
    user_id = models.IntegerField()
    title = models.CharField(max_length=256)
    body = models.TextField()
    extra_info = models.JSONField(null=True)