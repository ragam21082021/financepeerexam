import imp
import json
import pathlib
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views.generic import View
from rest_framework.response import Response
from visualize.models import visuvalizeData
from django.utils.decorators import method_decorator
from django.http import HttpRequest
# Create your views here.

@login_required
def home(request):
    return render(request,'base.html',{})


class uploadData(View):
    
    @method_decorator(login_required)
    def dispatch(self, request: HttpRequest):
        return super().dispatch(request)
    
    def get(self, request):
        upload_url = reverse('upload_view')
        return render(request,'visualize/upload.html',{'upload_url': upload_url})
    
    def post(self, request):
        if request.user.is_authenticated:
            try:
                file = request.FILES.get('jsonFile')
                if not file.name.endswith('.json'):
                    raise Exception('Please upload only Json Files')
                data = json.loads(file.read())
                for each in data:
                    self.check_and_save(each)
                return render(request,'visualize/upload.html',{'message':'Uploaded SuccessFully'})
            except Exception as e:
                return render(request,'visualize/upload.html',{'error':str(e)})
        else:
            return redirect('login_view')

    def check_and_save(self,data):
        userId = data.get('userId')
        title = data.get('title')
        body = data.get('body')
        record = visuvalizeData(
            user_id=userId,
            title=title,
            body=body
        )
        record.save()

@login_required
def data_view(request):
    data = visuvalizeData.objects.all()
    context = {'data':data}
    return render(request,'visualize/showData.html',context)
