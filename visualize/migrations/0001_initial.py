# Generated by Django 4.0.2 on 2022-02-16 20:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='visuvalizeData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('title', models.CharField(max_length=256)),
                ('body', models.TextField()),
                ('extra_info', models.JSONField()),
            ],
        ),
    ]
