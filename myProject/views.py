from multiprocessing import context
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def register(request):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        user_object = form.save()
        return redirect("/login")
    context = {"form": form}
    return render(request, "accounts/register.html", context)

def login_view(request):
    redirect_url = request.GET.get("next",['/'])[0]
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            user_object = form.get_user()
            login(request, user_object)
            return redirect(redirect_url)
    else:
        form = AuthenticationForm(request)
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect("/login")
    return render(request, "accounts/logout.html", {})
